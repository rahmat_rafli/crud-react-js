const TodoItem = ({ todo, setRefresh }) => {

    const updateTodo = () => {
        todo.done = !todo.done

        fetch("http://localhost:3001/todos/" + todo.id, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(todo)
        }).then(() => {
            alert('todo updated.')
            setRefresh(true)
        })
    }

    const deleteTodo = () => {
        fetch("http://localhost:3001/todos/" + todo.id, {
            method: 'DELETE'
        }).then(() => {
            alert('todo deleted.')
            setRefresh(true)
        })
    }

    return (
        <li className={`${todo.done ? "checked" : ""}`}>
            <div onClick={updateTodo}>{todo.title}</div>
            <span onClick={deleteTodo} className="close">x</span></li>
    );
}

export default TodoItem;